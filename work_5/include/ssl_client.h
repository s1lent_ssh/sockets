#include <openssl/ssl.h>
#include <openssl/err.h>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <experimental/filesystem>
#include <bitset>
#include <fstream>

#include "base_decoder.h"

enum class wait_policy {
	wait,
	no_wait
};

class ssl_exception : public std::runtime_error {
public:
	ssl_exception(const std::string& what) : runtime_error(what) {}
};

class socket_exception : public std::runtime_error {
public:
	socket_exception(const std::string& what) : runtime_error(what) {}
};

class ssl_client {
    const bool          DEBUG {true};
    SSL*                _ssl;
    SSL_CTX*            _context;
    const std::string 	_host;
	const size_t 		_port;
    const std::string 	EOL {"\r\n"};
	const std::string	KEY_PATH {"/home/s1lent/Documents/sockets/work_5/key/mycert.pem"};
	const size_t 		PACKAGE_SIZE {8192};

public:
    ssl_client(const std::string& host, size_t port);
    ~ssl_client();

    void init();
	void send_file(const std::string& path);
    std::string send(const std::string& data, const wait_policy& policy);
	std::string read();

    static std::string host_to_string(const std::string& host);
    static unsigned int host_to_ip(const std::string& host);
    static std::string ip_to_string(unsigned int ip);

private:
    void load_certificates(SSL_CTX* context, const std::string& certificate, const std::string& key);
	SSL_CTX* init_context();
    int create_socket();
	std::vector<char> get_package(const std::string& filename, size_t package_number);
};