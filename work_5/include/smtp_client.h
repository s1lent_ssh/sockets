#include <iostream>
#include <fstream>
#include <boost/algorithm/string/replace.hpp>

#include "base_decoder.h"
#include "color.h"
#include "ssl_client.h"
#include "letter.h"

class smtp_exception : public std::runtime_error {
  public:
    smtp_exception(const std::string &what) : runtime_error(what) {}
};

extern Color::Modifier red, green, def, blue;

class smtp_client {
    const std::string _email;
    const std::string _password;
    const std::string _name;
    const std::string _host;

    const std::string EOL{"\r\n"};
    const std::string BOUNDARY{"-------kek_LOL_ZDAROVA===------------"};
    const std::string ERROR_PREFIX{"error "};
    ssl_client _ssl_client;

  public:
    smtp_client(const std::string &host, size_t port, const std::tuple<std::string, std::string, std::string> login_data);
    void send_letter(const letter &letter);

  private:
    void send_smtp_command(const std::string &command, const wait_policy &policy = wait_policy::wait);
    void print_response(std::string response);
};