#include <string>
#include <vector>
#include <filesystem>

class letter {
	const std::string _to;
	const std::string _subject;
	const std::string _body;
	std::vector<std::filesystem::path> _attachments;

public:
	letter(const std::tuple<std::string, std::string, std::string>& letter_data);
	void set_attachments(const std::vector<std::filesystem::path>& attachments);
	const std::string& to() const;
	const std::string& subject() const;
	const std::string& body() const;
	const std::vector<std::filesystem::path>& attachments() const;
};