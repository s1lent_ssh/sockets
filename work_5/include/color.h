#include <iostream>

namespace Color {

enum Code {
	RED = 31,
	GREEN = 32,
	YELLOW = 33,
	BLUE = 34,
	MAGENTA = 35,
	CYAN = 36,
	LIGHT_GRAY = 37,
	DEFAULT = 39,
	BOLD_RED = 131
};

class Modifier {
public:
	Code code;
	Modifier(const Code& code);
	friend std::ostream& operator<<(std::ostream& os, const Modifier& m);
};

}