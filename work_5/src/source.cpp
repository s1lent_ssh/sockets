#include <filesystem>
#include "../include/smtp_client.h"
#include "../include/account_manager.h"

const std::string host {"smtp.yandex.ru"};
const unsigned port {465};

std::tuple<std::string, std::string, std::string> login_prompt() {
	std::string email, password, name;
	std::cout << red << "email > " << def; std::getline(std::cin, email);
	std::cout << red << "pass  > " << def; std::getline(std::cin, password);
	std::cout << red << "name  > " << def; std::getline(std::cin, name);
	return { email, password, name }; 
}

std::tuple<std::string, std::string, std::string> letter_prompt() {
	std::string to, subject, body;
	std::cout << blue << "to: " << def; std::getline(std::cin, to);
	std::cout << blue << "subject: " << def; std::getline(std::cin, subject);
	std::cout << blue << "body: " << def; std::getline(std::cin, body);
	return { to, subject, body };
}

std::vector<std::filesystem::path> attachment_prompt() {
	std::vector<std::filesystem::path> attachments;
	std::string add;

	while(true) {
		std::cout << blue << "add attachment? [y/N]: " << def;
		std::getline(std::cin, add);
		if(add == "y") {
			std::string attachment;
			std::cout << blue << "path: " << def; std::getline(std::cin, attachment);
			std::filesystem::path attachment_path(attachment);
			if(!std::filesystem::exists(attachment_path)) {
				std::cerr << red << "no such file" << std::endl;
			} else {
				attachments.push_back(attachment_path);
			}
		} else {
			break;
		}
	}
	return attachments;
}

auto main() -> int {
	account_manager account;

	std::tuple<std::string, std::string, std::string> account_data;
	try {
		account_data = account.get_account_data();
		std::cout << green << "loaded " << def << "session for " << std::get<0>(account_data) << " (" << std::get<2>(account_data) << ")" << std::endl;
	} catch(const std::exception& ex) {
		std::cerr << red << "no session stored. " << def << " please log in" << std::endl;
		account_data = login_prompt();
		account.set_account_data(account_data);
	}

	smtp_client client(host, port, account_data);
	letter my_letter(letter_prompt());
	my_letter.set_attachments(attachment_prompt());
	client.send_letter(my_letter);
}