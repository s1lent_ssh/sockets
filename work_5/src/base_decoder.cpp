#include "../include/base_decoder.h"

static const std::string base64_chars =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz"
    "0123456789+/";

std::string encode_base64(const std::string& data) {
	std::vector<unsigned char> bytes(data.begin(), data.end());
	std::vector<unsigned char> result;

	for (auto byte = bytes.begin(); byte != bytes.end(); byte += 3) {
		if (byte - 1 == bytes.end() || byte - 2 == bytes.end()) {
			break;
		}

		result.push_back(base64_chars.at(byte[0] >> 2));

		if (byte + 1 != bytes.end()) {
			result.push_back(base64_chars.at( ((byte[0] << 4 ) | (byte[1] >> 4)) & 0b00111111));
		} else {
			result.push_back(base64_chars.at( (byte[0] << 4) & 0b00111111));
			result.push_back('=');
			result.push_back('=');
			break;
		}

		if (byte + 2 != bytes.end()) {
			result.push_back(base64_chars.at( ((byte[1] << 2) | (byte[2] >> 6)) & 0b00111111 ));
			result.push_back(base64_chars.at( byte[2] & 0b00111111 ));
		} else {
			result.push_back(base64_chars.at( (byte[1] << 2) & 0b00111111 ));
			result.push_back('=');
			break;
		}
	}
	return std::string(result.begin(), result.end());
}
