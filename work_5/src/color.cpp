#include "../include/color.h"

namespace Color {

Modifier::Modifier(const Code &code) : code(code) {}

std::ostream& operator<<(std::ostream &os, const Modifier &m) {
    if (m.code > 100) {
        os << "\033[1;" << m.code - 100 << "m";
    } else {
        os << "\033[0;" << m.code << "m";
    }
    return os;
}

} // namespace Color