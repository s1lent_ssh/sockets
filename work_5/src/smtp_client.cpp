#include "../include/smtp_client.h"

Color::Modifier red(Color::RED), green(Color::GREEN), def(Color::DEFAULT), blue(Color::BLUE);

smtp_client::smtp_client(const std::string &host, size_t port, const std::tuple<std::string, std::string, std::string> login_data) : 
	_email(std::get<0>(login_data)), _password(std::get<1>(login_data)), _name(std::get<2>(login_data)), _host(host), _ssl_client(host, port) {}

void smtp_client::send_letter(const letter &letter) {
	try {
		std::cout << green << "connecting " << def << "to " << _host << " (" << ssl_client::host_to_string(_host) << ")" << std::endl;
		_ssl_client.init();
		std::cout << green << "ssl " << def << "connection established" << std::endl;
	} catch (const smtp_exception &ex) {
		std::cerr << red << ERROR_PREFIX << def << ex.what() << std::endl;
		return;
	}

	_ssl_client.read();
	try {
		send_smtp_command("EHLO " + _host);
		send_smtp_command("AUTH LOGIN");
		send_smtp_command(encode_base64(_email));
		send_smtp_command(encode_base64(_password));
		send_smtp_command("MAIL FROM:<" + _email + ">");
		send_smtp_command("RCPT TO:<" + letter.to() + ">");
		send_smtp_command("DATA");

		send_smtp_command("From: \"" + _name + "\" <" + _email + ">", wait_policy::no_wait);
		send_smtp_command("To: <" + letter.to() + ">", wait_policy::no_wait);
		send_smtp_command("Subject: " + letter.subject(), wait_policy::no_wait);
		
		if(letter.attachments().empty()) {
			send_smtp_command("Content-Transfer-Encoding: base64", wait_policy::no_wait);
			send_smtp_command("Content-Type: text/plain; charset=ascii-us", wait_policy::no_wait);
		} else {
			send_smtp_command("Content-Type: multipart/mixed;", wait_policy::no_wait);
			send_smtp_command("\tboundary=\"" + BOUNDARY + "\"", wait_policy::no_wait);
		}
		send_smtp_command("", wait_policy::no_wait);

		if(!letter.attachments().empty()) {
			send_smtp_command("--" + BOUNDARY, wait_policy::no_wait);
			send_smtp_command("Content-Transfer-Encoding: 7bit", wait_policy::no_wait);
			send_smtp_command("Content-Type: text/plain;", wait_policy::no_wait);
			send_smtp_command("", wait_policy::no_wait);
		}

		send_smtp_command(letter.body(), wait_policy::no_wait);

		for (const auto &attachment : letter.attachments()) {
			send_smtp_command("--" + BOUNDARY, wait_policy::no_wait);
			send_smtp_command("Content-Transfer-Encoding: base64", wait_policy::no_wait);
			send_smtp_command("Content-Type: application; name=\"" + attachment.filename().string() + "\"", wait_policy::no_wait);
			send_smtp_command("Content-Disposition: attachment;", wait_policy::no_wait);
			send_smtp_command("", wait_policy::no_wait);
			_ssl_client.send_file(attachment);
		}

		if(!letter.attachments().empty()) {
			send_smtp_command("--" + BOUNDARY, wait_policy::no_wait);
		}

		send_smtp_command(".");
		send_smtp_command("QUIT");
		std::cout << green << "message " << def << "sent." << std::endl;
	} catch (const smtp_exception &ex) {
		std::cerr << red << ERROR_PREFIX << def << ex.what() << std::endl;
	}
}

void smtp_client::send_smtp_command(const std::string &command, const wait_policy &policy) {
	std::cout << command << std::endl;
	switch (policy) {
		case wait_policy::wait: {
			auto response = _ssl_client.send(command, policy);
			int code = std::stoi(response.substr(0, 3));
			if (code >= 400) {
				throw smtp_exception(response);
			}
			print_response(response);
		break;
		}
		case wait_policy::no_wait:
			_ssl_client.send(command, policy);
	}
}

void smtp_client::print_response(std::string response) {
	for (size_t position = 0; position != std::string::npos;) {
		auto next_gap = response.find("\n", position + 1);
		if (next_gap != std::string::npos && next_gap - position < 40) {
			position = next_gap;
		} else {
			position = response.find(" ", position + 40);
			if (position != std::string::npos) {
				response.replace(position, 1, "\n");
			}
		}
	}
	boost::replace_all(response, "\n", "\n      ");
	boost::replace_last(response, "\n      ", "\n");
	std::cout << green << "  <-  " << def << response;
}
