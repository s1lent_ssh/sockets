#include "../include/letter.h"

letter::letter(const std::tuple<std::string, std::string, std::string>& letter_data)
	: _to(std::get<0>(letter_data)), _subject(std::get<1>(letter_data)), _body(std::get<2>(letter_data)) {}

void letter::set_attachments(const std::vector<std::filesystem::path>& attachments) {
	_attachments = attachments;
}

const std::string& letter::to() const {
	return _to;
}

const std::string& letter::subject() const {
	return _subject;
}

const std::string& letter::body() const {
	return _body;
}

const std::vector<std::filesystem::path>& letter::attachments() const {
	return _attachments;
}
