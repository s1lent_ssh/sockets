#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <istream>
#include <iterator>

using namespace std;

const int PORT {1100};
const int BUFFER_SIZE {32};

string file2str(const string& filename) {
	fstream file(filename, ios::binary | ios::in);
	std::string data;
	file.seekg(0, ios::end);   
	data.reserve(file.tellg());
	file.seekg(0, ios::beg);
	data.assign(
		(std::istreambuf_iterator<char>(file)),
		std::istreambuf_iterator<char>()
	);
	return data;
}

int main() {
	sockaddr_in _sockaddr {
		AF_INET,
		htons(PORT), 
		{htonl(INADDR_LOOPBACK)}
	};

	int _socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(_socket < 0) {
		cerr << "[Client] Socket create error" << endl;
		return 1;
	}

	int _connect = connect(
		_socket, 
		reinterpret_cast<sockaddr*>(&_sockaddr), 
		sizeof(_sockaddr)
	);
	if(_connect < 0) {
		cerr << "[Client] Socket connect error" << endl;
		close(_socket);
		return 1;
	}

	string filename;
	cout << "enter filename to send > "; cin >> filename;
	auto _data = file2str(filename);
	send(_socket, _data.c_str(), _data.size(), 0);

	close(_socket);
}