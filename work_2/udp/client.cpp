#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <iostream>
#include <fstream>

constexpr int PORT {7779};

class UdpFileDownloader {

	static const int BUFFER_SIZE {512};
	sockaddr_in _sockaddr;
	int _socket;
	char buffer[BUFFER_SIZE];

public:
	UdpFileDownloader(size_t port) {
		_sockaddr = {
			AF_INET,
			htons(port),
			{htonl(INADDR_LOOPBACK)}
		};
		_socket = socket(AF_INET, SOCK_DGRAM, 0);
		if (_socket < 0) {
			std::cout << "[Client] Socket create error" << std::endl;
		}
	}

	~UdpFileDownloader() {
		shutdown(_socket, 2);
		close(_socket);
	}

	std::string make_request(const std::string& request) {
		std::cout << " <- " << request << std::endl;
		sendto(_socket, request.c_str(), request.size(), 0, reinterpret_cast<sockaddr*>(&_sockaddr), sizeof(_sockaddr));
		int readed = recvfrom(_socket, buffer, sizeof(buffer), 0, nullptr, nullptr);
		std::string response = std::string(buffer, buffer + readed);
		return response;
	}

	bool check_file(const std::string& filename) {
		std::string request = "check " + filename;
		std::string response = make_request(request);
		std::cout << " -> " << response << std::endl; 
		if(response == "exists") {
			return true;
		} else {
			return false;
		}
	}

	size_t get_file_size(const std::string& filename) {
		std::string request = "size " + filename;
		std::string response = make_request(request);
		std::cout << " -> " << response << std::endl; 
		size_t size;
		sscanf(response.c_str(), "%zu", &size);
		return size;
	}

	void request_package_size(size_t size) {
		std::string request = "package_size " + std::to_string(size);
		make_request(request);
	}

	std::string request_package(const std::string& filename, size_t number) {
		std::string request = "request_package " + filename + " " + std::to_string(number);
		return make_request(request);
	}

	void download_file(const std::string& filename) {
		if(!check_file(filename)) {
			return;
		}
		size_t file_size = get_file_size(filename);
		request_package_size(sizeof(buffer));

		size_t packages_count = file_size / BUFFER_SIZE + 1;
		std::ofstream file("copy " + filename, std::ios::out | std::ios::app | std::ios::binary);
		for(size_t i = 0; i < packages_count; i++) {
			std::string data = request_package(filename, i);
			std::cout << " -> got " << i << " package of " << packages_count - 1 << std::endl;
			file << data;
		}
		file.close();
	}
};

auto main() -> decltype(static_cast<const int>(sizeof("kek"))) {
	UdpFileDownloader downloader(PORT);

	std::string filename;
	std::cout << "file to download > "; std::cin >> filename;

	downloader.download_file(filename);
}