#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <iterator>
#include <sys/stat.h>
#include <fstream>

using std::cerr;
using std::cout;
using std::string;
using std::endl;

constexpr int BUFFER_SIZE {512};
constexpr int PORT {7779};

size_t package_size {0};

bool check_file(const std::string& filename) {
	cout << "(server) checking file " << filename << endl;
	struct stat buffer;   
  	return (stat (filename.c_str(), &buffer) == 0); 
}

size_t file_size(const std::string& filename) {
	struct stat st;
	stat(filename.c_str(), &st);
	return st.st_size;
}

std::vector<char> get_package(const std::string& filename, size_t package_number) {
	std::ifstream file(filename, std::ifstream::binary | std::ios::out);
	std::vector<char> buffer(package_size);
	if(file) {
		file.seekg(0, std::ios::end);
		size_t end = file.tellg();
		file.seekg(package_number * package_size, std::ios::beg);
		size_t distance_to_end = end - file.tellg();
		if(distance_to_end < package_size) {
			buffer.resize(distance_to_end);
			file.read (buffer.data(), distance_to_end);
		} else {
			file.read (buffer.data(), package_size);
		}
		file.close();
	}
	return buffer;
}

auto main() -> decltype(228) {
	sockaddr_in _sockaddr {
		AF_INET,
		htons(PORT),
		{htonl(INADDR_LOOPBACK)}
	};

	int _socket = socket(AF_INET, SOCK_DGRAM, 0);
	if (_socket < 0) {
		cerr << "[Server] Socket create error" << endl;
		shutdown(_socket, 2);
		close(_socket);
		return 1;
	}

	int _bind = bind(
	                _socket,
	                reinterpret_cast<sockaddr*>(&_sockaddr),
	                sizeof(_sockaddr)
	            );
	if (_bind < 0) {
		cerr << "[Server] Socket bind error" << endl;
		shutdown(_socket, 2);
		close(_socket);
		return 1;
	}

	char buffer[BUFFER_SIZE];

	sockaddr_in _client_sockaddr {
		AF_INET,
		htons(PORT),
		{htonl(INADDR_LOOPBACK)}
	};

	while(true) {
		unsigned size = sizeof(_client_sockaddr);
		int readed = recvfrom(_socket, buffer, sizeof(buffer), 0, reinterpret_cast<sockaddr*>(&_client_sockaddr), &size);
		string response = string(buffer, buffer + readed);
		
		cout << " -> " << response << endl;
		std::istringstream buf(response);
    	std::istream_iterator<std::string> beg(buf), end;
    	std::vector<std::string> tokens(beg, end);

    	const auto& command = tokens.at(0);

		if(command == "check") {
			if(check_file(tokens.at(1))) {
				cout << "(server) file found" << endl;
				std::string data = "exists";
				sendto(_socket, data.c_str(), data.size(), 0, reinterpret_cast<sockaddr*>(&_client_sockaddr), size);
			} else {
				cout << "(server) file not found" << endl;
				std::string data = "not exists";
				sendto(_socket, data.c_str(), data.size(), 0, reinterpret_cast<sockaddr*>(&_client_sockaddr), size);
			}
		} else if(command == "size") {
			size_t f_size = file_size(tokens.at(1));
			std::string data = std::to_string(f_size);
			cout << "(server) file size: " << data << endl;
			sendto(_socket, data.c_str(), data.size(), 0, reinterpret_cast<sockaddr*>(&_client_sockaddr), size);
		} else if(command == "package_size") {
			cout << "(server) set package_size to " << tokens.at(1) << endl;
			sscanf(tokens.at(1).c_str(), "%zu", &package_size);
			std::string data = "successfully set to " + tokens.at(1);
			sendto(_socket, data.c_str(), data.size(), 0, reinterpret_cast<sockaddr*>(&_client_sockaddr), size);
		} else if(command == "request_package") {
			std::string filename = tokens.at(1);
			size_t package_number;
			sscanf(tokens.at(2).c_str(), "%zu", &package_number);
			auto data = get_package(filename, package_number);
			int sent = sendto(_socket, data.data(), data.size(), 0, reinterpret_cast<sockaddr*>(&_client_sockaddr), size);
			cout << "(server) sent " << sent << " bytes" << endl;
		}
	}

	shutdown(_socket, 2);
	close(_socket);
}