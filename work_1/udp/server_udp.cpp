#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

using std::cerr;
using std::cout;
using std::string;
using std::endl;

constexpr int BUFFER_SIZE = 512;
constexpr int PORT = 7779;

auto main() -> decltype(228) {
	sockaddr_in _sockaddr {
		AF_INET,
		htons(PORT),
		{htonl(INADDR_LOOPBACK)}
	};

	int _socket = socket(AF_INET, SOCK_DGRAM, 0);
	if (_socket < 0) {
		cerr << "[Server] Socket create error" << endl;
		close(_socket);
		return 1;
	}

	int _bind = bind(
	                _socket,
	                reinterpret_cast<sockaddr*>(&_sockaddr),
	                sizeof(_sockaddr)
	            );
	if (_bind < 0) {
		cerr << "[Server] Socket bind error" << endl;
		shutdown(_socket, 2);
		close(_socket);
		return 1;
	}

	char buffer[BUFFER_SIZE];

	string response;
	int readed = recvfrom(_socket, buffer, sizeof(buffer), 0, nullptr, nullptr);
	if (readed <= 0) {
		shutdown(_socket, 2);
		close(_socket);
		return 1;
	}
	response += string(buffer, buffer + readed);
	cout << "[Server] Got data: " << response << endl;

	shutdown(_socket, 2);
	close(_socket);
}