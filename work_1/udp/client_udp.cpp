#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <iostream>

using std::cerr;
using std::cout;
using std::string;
using std::endl;

constexpr int PORT = 7779;

auto main() -> decltype(static_cast<const int>(sizeof("kek"))) {
	sockaddr_in _sockaddr {
		AF_INET,
		htons(PORT),
		{htonl(INADDR_LOOPBACK)}
	};

	int _socket = socket(AF_INET, SOCK_DGRAM, 0);
	if (_socket < 0) {
		cout << "[Client] Socket create error" << endl;
		return 1;
	}

	string _data = "Message";

	sendto(_socket, _data.c_str(), _data.size(), 0, reinterpret_cast<sockaddr*>(&_sockaddr), sizeof(_sockaddr));
	cout << "[Client] Send data: " + _data << endl;


	shutdown(_socket, 2);
	close(_socket);
}