#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <signal.h>

using std::cerr;
using std::cout;
using std::string;
using std::endl;

constexpr int BUFFER_SIZE = 1;
constexpr int PORT = 7777;

auto main() -> decltype(228) {
	sockaddr_in _sockaddr {
		AF_INET,
		htons(PORT),
		{htonl(INADDR_LOOPBACK)}
	};

	int _socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket < 0) {
		cerr << "[Server] Socket create error" << endl;
		close(_socket);
		return 1;
	}

	int _bind = bind(
	                _socket,
	                reinterpret_cast<sockaddr*>(&_sockaddr),
	                sizeof(_sockaddr)
	            );
	if (_bind < 0) {
		cerr << "[Server] Socket bind error" << endl;
		close(_socket);
		return 1;
	}

	int _listen = listen(_socket, 10);
	if (_listen < 0) {
		cerr << "[Server] Socket listen error" << endl;
		close(_socket);
		return 1;
	}

	char buffer[BUFFER_SIZE];


	int _connection = accept(_socket, NULL, NULL);
	if (_connection < 0) {
		cerr << "[Server] Socket accept error" << endl;
		return 1;
	}
	string response;
	while (true) {
		int readed = recv(_connection, buffer, sizeof(buffer), 0);
		if (readed <= 0) break;
		response += string(buffer, buffer + readed);
	}
	cout << "[Server] Got data: " << response << endl;

	shutdown(_socket, 2);
	close(_socket);
}