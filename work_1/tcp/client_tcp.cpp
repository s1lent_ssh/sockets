#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <iostream>

using std::cerr;
using std::cout;
using std::string;
using std::endl;

constexpr int PORT = 7777;

auto main() -> decltype(static_cast<const int>(sizeof("kek"))) {
	sockaddr_in _sockaddr {
		AF_INET,
		htons(PORT), 
		{htonl(INADDR_LOOPBACK)}
	};

	int _socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(_socket < 0) {
		cerr << "[Client] Socket create error" << endl;
		return 1;
	}

	int _connect = connect(
		_socket, 
		reinterpret_cast<sockaddr*>(&_sockaddr), 
		sizeof(_sockaddr)
	);
	if(_connect < 0) {
		cerr << "[Client] Socket connect error" << endl;
		shutdown(_socket);
		close(_socket);
		return 1;
	}

	string _data = "Message";

	send(_socket, _data.c_str(), _data.size(), 0);
	cout << "[Client] Send data: " + _data << endl;

	shutdown(_socket, 2);
	close(_socket);
}