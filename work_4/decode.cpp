#include <iostream>
#include <vector>

using std::string;
using std::cout;
using std::endl;
using std::vector;
using std::cin;

static const std::string base64_chars =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz"
    "0123456789+/";

template<typename... Item>
void push_back(vector<char>& data, Item... item) {
	(data.push_back(item), ...);
}

std::string decode(const string& data) {
	vector<char> bytes(data.begin(), data.end());
	vector<char> result;
	
	for (auto byte = bytes.begin(); byte != bytes.end(); byte += 3) {

		if (byte - 1 == bytes.end() || byte - 2 == bytes.end()) {
			break;
		}

		result.push_back(
			base64_chars.at(byte[0] >> 2)
		);

		if (byte + 1 != bytes.end()) {
			result.push_back(
				base64_chars.at( ((byte[0] << 4 ) | (byte[1] >> 4)) & 0b00111111) 
			);
		} else {
			push_back(result, 
				base64_chars.at( (byte[0] << 4) & 0b00111111 ), 
				'=', 
				'='
			);
			break;
		}

		if (byte + 2 != bytes.end()) {
			push_back(result, 
				base64_chars.at( ((byte[1] << 2) | (byte[2] >> 6)) & 0b00111111 ), 
				base64_chars.at( byte[2] & 0b00111111 )
			);
		} else {
			push_back(result, 
				base64_chars.at( (byte[1] << 2) & 0b00111111 ), 
				'='
			);
			break;
		}
	}
	return string(result.begin(), result.end());
}

std::string encode(const string& data) {
	vector<char> bytes(data.begin(), data.end());
	vector<char> result;

	for (auto byte = bytes.begin(); byte != bytes.end(); byte += 4) {
		result.push_back(
			(base64_chars.find(byte[0]) << 2) | (base64_chars.find(byte[1]) >> 4)
		);

		if(byte[2] != '=') {
			result.push_back(
				(base64_chars.find(byte[1]) << 4) | (base64_chars.find(byte[2]) >> 2)
			);
		} else {
			push_back(result, 
				base64_chars.find(byte[1]) << 4, 
				0
			);
			break;
		}

		if(byte[3] != '=') {
			result.push_back(
				(base64_chars.find(byte[2]) << 6) | base64_chars.find(byte[3])
			);
		} else {
			result.push_back(
				base64_chars.find(byte[2]) << 6
			);
			break;
		}
	}
	return string(result.begin(), result.end());
}

auto main() -> int {
	string input;
	while(true) {
		cout << "\033[34;1mdecode > \033[39;0m"; cin >> input;
		string decoded = decode(input);
		cout << input << " -> " << decoded << endl;
		cout << decoded << " -> " << encode(decoded) << endl;
	}
}