#include <string>
#include <vector>

std::string encode_base64(const std::string& data);
std::string decode_base64(const std::string& data);