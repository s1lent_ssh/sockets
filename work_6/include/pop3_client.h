#include <iostream>
#include <fstream>
#include <boost/algorithm/string/replace.hpp>
#include <string_view>

#include "base_decoder.h"
#include "color.h"
#include "ssl_client.h"

class pop3_exception : public std::runtime_error {
  public:
    pop3_exception(const std::string &what) : runtime_error(what) {}
};

extern Color::Modifier red, green, def, blue;

class pop3_client {
    const std::string _email;
    const std::string _password;
    const std::string _name;
    const std::string _host;

    const std::string EOL{"\r\n"};
    const std::string BOUNDARY{"kek"};
    const std::string ERROR_PREFIX{"error "};
    ssl_client _ssl_client;

  public:
    pop3_client(const std::string &host, size_t port, const std::tuple<std::string, std::string, std::string> login_data);
    void get_letters();

  private:
    void send_pop3_command(const std::string &command, const wait_policy &policy = wait_policy::wait);
    void print_response(std::string response);
    void retrieve_message(size_t id);
    void retrieve_message_part(const std::string& boundary);

    std::string extract_field(std::string_view);
    std::string extract_body(std::string_view boundary);
};