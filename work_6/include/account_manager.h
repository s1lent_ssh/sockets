#include <sqlite_modern_cpp.h>

class account_manager {
    sqlite::database db;

public:
    account_manager();
    std::tuple<std::string, std::string, std::string> get_account_data();
    void set_account_data(std::tuple<std::string, std::string, std::string> data);
};