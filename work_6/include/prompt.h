#include <tuple>
#include <vector>
#include <filesystem>
#include "color.h"

std::tuple<std::string, std::string, std::string> login_prompt();

std::tuple<std::string, std::string, std::string> letter_prompt();

std::vector<std::filesystem::path> attachment_prompt();