#include "../include/pop3_client.h"
#include "../include/account_manager.h"
#include "../include/prompt.h"

const std::string host {"pop.yandex.ru"};
const unsigned port {995};

auto main() -> int {
	account_manager account;

	std::tuple<std::string, std::string, std::string> account_data;
	try {
		account_data = account.get_account_data();
		std::cout << green << "loaded " << def << "session for " << std::get<0>(account_data) << " (" << std::get<2>(account_data) << ")" << std::endl;
	} catch(const std::exception& ex) {
		std::cerr << red << "no session stored. " << def << " please log in" << std::endl;
		account_data = login_prompt();
		account.set_account_data(account_data);
	}

	pop3_client client(host, port, account_data);
	client.get_letters();
}