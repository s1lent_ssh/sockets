#include "../include/base_decoder.h"

static const std::string base64_chars =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz"
    "0123456789+/";

std::string encode_base64(const std::string& data) {
	std::vector<unsigned char> bytes(data.begin(), data.end());
	std::vector<unsigned char> result;

	for (auto byte = bytes.begin(); byte != bytes.end(); byte += 3) {
		if (byte - 1 == bytes.end() || byte - 2 == bytes.end()) {
			break;
		}

		result.push_back(base64_chars.at(byte[0] >> 2));

		if (byte + 1 != bytes.end()) {
			result.push_back(base64_chars.at( ((byte[0] << 4 ) | (byte[1] >> 4)) & 0b00111111));
		} else {
			result.push_back(base64_chars.at( (byte[0] << 4) & 0b00111111));
			result.push_back('=');
			result.push_back('=');
			break;
		}

		if (byte + 2 != bytes.end()) {
			result.push_back(base64_chars.at( ((byte[1] << 2) | (byte[2] >> 6)) & 0b00111111 ));
			result.push_back(base64_chars.at( byte[2] & 0b00111111 ));
		} else {
			result.push_back(base64_chars.at( (byte[1] << 2) & 0b00111111 ));
			result.push_back('=');
			break;
		}
	}
	return std::string(result.begin(), result.end());
}

std::string decode_base64(const std::string& data) {
	std::vector<char> bytes(data.begin(), data.end());
	std::vector<char> result;

	for (auto byte = bytes.begin(); byte != bytes.end(); byte += 4) {
		if (byte - 1 == bytes.end() || byte - 2 == bytes.end() || byte - 3 == bytes.end()) {
			break;
		}

		result.push_back(
			(base64_chars.find(byte[0]) << 2) | (base64_chars.find(byte[1]) >> 4)
		);

		if(byte[2] != '=') {
			result.push_back((base64_chars.find(byte[1]) << 4) | (base64_chars.find(byte[2]) >> 2));
		} else {
			result.push_back(base64_chars.find(byte[1]) << 4);
			result.push_back(0);
			break;
		}

		if(byte[3] != '=') {
			result.push_back((base64_chars.find(byte[2]) << 6) | base64_chars.find(byte[3]));
		} else {
			result.push_back(base64_chars.find(byte[2]) << 6);
			break;
		}
	}
	return std::string(result.begin(), result.end());
}
