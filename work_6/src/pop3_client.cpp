#include "../include/pop3_client.h"

pop3_client::pop3_client(const std::string &host, size_t port, const std::tuple<std::string, std::string, std::string> login_data) : 
	_email(std::get<0>(login_data)), _password(std::get<1>(login_data)), _name(std::get<2>(login_data)), _host(host), _ssl_client(host, port) {}

void pop3_client::get_letters() {
	try {
		std::cout << green << "connecting " << def << "to " << _host << " (" << ssl_client::host_to_string(_host) << ")" << std::endl;
		_ssl_client.init();
		std::cout << green << "ssl " << def << "connection established" << std::endl;
	} catch (const pop3_exception &ex) {
		std::cerr << red << ERROR_PREFIX << def << ex.what() << std::endl;
		return;
	}

	_ssl_client.read();
	try {
		send_pop3_command("USER " + _email);
		send_pop3_command("PASS " + _password);
		send_pop3_command("STAT");
		retrieve_message(1);
	} catch (const pop3_exception &ex) {
		std::cerr << red << ERROR_PREFIX << def << ex.what() << std::endl;
	}
}

std::string pop3_client::extract_field(std::string_view field) {
	for(std::string data; data != ".\r\n"; data = _ssl_client.read()) {
		if(size_t start_position = data.find(field); start_position != std::string::npos) {
			return data.substr(field.size(), data.size() - field.size() - 2);
		}
	}
	throw pop3_exception("end_of_message_exception");
	return "";
}

std::string pop3_client::extract_body(std::string_view boundary) {
	for(std::string data; data != ".\r\n"; data = _ssl_client.read()) {
		if(data == "\r\n") {
			break;
		}
	}
	std::string result;
	for(std::string data; data.find(boundary) == std::string::npos; data = _ssl_client.read()) {
		result += data.substr(0, data.size() - 2);
	}
	return result;
}

void pop3_client::retrieve_message_part(const std::string& boundary) {
	//Get headers
	auto content_transfer_encoding = extract_field("Content-Transfer-Encoding: ");
	auto content_type = extract_field("Content-Type: ");

	//Get body
	auto body = extract_body(boundary);

	if(content_type.find("image/jpeg;") != std::string::npos) {
		//Create file
		std::ofstream file("message_attachment");
		if(file.is_open()) {
			file << decode_base64(body);
			std::cout << red << "attachment saved" << std::endl;
		} else {
			std::cerr << "file error" << std::endl;
		}
		file.close();
	} else if (content_type.find("text/plain") != std::string::npos) {
		//Print text
		std::cout << blue << "body: " << def << body << std::endl;
	}
}

void pop3_client::retrieve_message(size_t id) {
	//Send command;
	send_pop3_command("RETR " + std::to_string(id));
	//Extract from
	auto from = extract_field("From: ");
	size_t open_brace_pos = from.find_last_of("<");
	size_t close_brace_pos = from.find_last_of(">");
	from = from.substr(open_brace_pos + 1, close_brace_pos - open_brace_pos - 1);
	//Extract subject
	auto subject = extract_field("Subject: ");
	//Extract content type and boundary
	auto content_type = extract_field("Content-Type: ");

	std::cout << blue << "from: " << def << from << std::endl;
	std::cout << blue << "subject: " << def << subject << std::endl;

	if(content_type == "multipart/mixed;") {
		auto boundary = extract_field("\tboundary=");
		boundary = boundary.substr(1, boundary.size() - 2);
		while(true) {
			try {
				retrieve_message_part(boundary);
			} catch (const std::exception& ex) {
				break;
			}
		}
	} else {
		//Its only text
	}
}

void pop3_client::send_pop3_command(const std::string& command, const wait_policy &policy) {
	//std::cout << " -> " << command << std::endl;
	switch (policy) {
		case wait_policy::wait: {
			auto response = _ssl_client.send(command, policy);
			//print_response(response);
		break;
		}
		case wait_policy::no_wait:
			_ssl_client.send(command, policy);
	}
}

void pop3_client::print_response(std::string response) {
	for (size_t position = 0; position != std::string::npos;) {
		auto next_gap = response.find("\n", position + 1);
		if (next_gap != std::string::npos && next_gap - position < 40) {
			position = next_gap;
		} else {
			position = response.find(" ", position + 40);
			if (position != std::string::npos) {
				response.replace(position, 1, "\n");
			}
		}
	}
	boost::replace_all(response, "\n", "\n      ");
	boost::replace_last(response, "\n      ", "\n");
	std::cout << green << "  <-  " << def << response;
}
