#include "../include/account_manager.h"

account_manager::account_manager() : db("/home/s1lent/Documents/sockets/work_5/database/mail.db") {
	db <<
	    "create table if not exists account ("
	    "id integer primary key not null,"
	    "email text,"
	    "password text,"
		"name text"
	    ");";
}

std::tuple<std::string, std::string, std::string> account_manager::get_account_data() {
	std::string email, password, name;
	db 	<< "select email from account where id = 0;" >> [&](const std::string& _email) {
		email = _email;
	};
	db 	<< "select password from account where id = 0;" >> [&](const std::string& _password) {
		password = _password;
	};
	db 	<< "select name from account where id = 0;" >> [&](const std::string& _name) {
		name = _name;
	};
	if(email.empty() || password.empty() || name.empty()) {
		throw std::runtime_error("Database empty");
	}
	return {email, password, name};
}

void account_manager::set_account_data(std::tuple<std::string, std::string, std::string> data) {
	db 	<< "insert into account (id, email, password, name) values (?, ?, ?, ?);" 
		<< 0 
		<< std::get<0>(data) 
		<< std::get<1>(data)
		<< std::get<2>(data);
} 