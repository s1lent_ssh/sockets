#include "../include/prompt.h"

std::tuple<std::string, std::string, std::string> login_prompt() {
	std::string email, password, name;
	std::cout << red << "email > " << def; std::getline(std::cin, email);
	std::cout << red << "pass  > " << def; std::getline(std::cin, password);
	std::cout << red << "name  > " << def; std::getline(std::cin, name);
	return { email, password, name }; 
}

std::tuple<std::string, std::string, std::string> letter_prompt() {
	std::string to, subject, body;
	std::cout << blue << "to: " << def; std::getline(std::cin, to);
	std::cout << blue << "subject: " << def; std::getline(std::cin, subject);
	std::cout << blue << "body: " << def; std::getline(std::cin, body);
	return { to, subject, body };
}

std::vector<std::filesystem::path> attachment_prompt() {
	std::vector<std::filesystem::path> attachments;
	std::string add;

	while(true) {
		std::cout << blue << "add attachment? [y/N]: " << def;
		std::getline(std::cin, add);
		if(add == "y") {
			std::string attachment;
			std::cout << blue << "path: " << def; std::getline(std::cin, attachment);
			std::filesystem::path attachment_path(attachment);
			if(!std::filesystem::exists(attachment_path)) {
				std::cerr << red << "no such file" << std::endl;
			} else {
				attachments.push_back(attachment_path);
			}
		} else {
			break;
		}
	}
	return attachments;
}