#include "../include/ssl_client.h"

ssl_client::ssl_client(const std::string &host, size_t port) : _host(host), _port(port) {}

ssl_client::~ssl_client() {
	int ssl_socket = SSL_get_fd(_ssl);
	SSL_free(_ssl);
	SSL_CTX_free(_context);
	shutdown(ssl_socket, 2);
	close(ssl_socket);
}

void ssl_client::init() {
	_context = init_context();
	load_certificates(_context, KEY_PATH.c_str(), KEY_PATH.c_str());
	_ssl = SSL_new(_context);
	SSL_set_fd(_ssl, create_socket());
	if (SSL_connect(_ssl) <= 0) {
		throw ssl_exception("Can't make ssl connection");
	}
}

void ssl_client::send_file(const std::string &path) {
	size_t iterations = std::experimental::filesystem::file_size(path) / PACKAGE_SIZE + 1;
	for (size_t i = 0; i < iterations; i++) {
		auto package = get_package(path, i);
		auto data = encode_base64(std::string(package.begin(), package.end()));
		int sent = SSL_write(_ssl, data.data(), data.size());
		(void)sent;
	}
}

std::string ssl_client::send(const std::string &data, const wait_policy &policy) {
	std::string _data = data + EOL;
	int sent = SSL_write(_ssl, _data.c_str(), _data.size());
	(void)sent;
	switch (policy) {
		case wait_policy::wait:
			return read();
		case wait_policy::no_wait:
			return "";
		default:
			return "";
	}
}

std::string ssl_client::read() {
	char buffer[1024];
	int readed = SSL_read(_ssl, buffer, sizeof(buffer));
	return std::string(buffer, buffer + readed);
}

std::string ssl_client::host_to_string(const std::string &host) {
	return ip_to_string(host_to_ip(host));
}

unsigned int ssl_client::host_to_ip(const std::string &host) {
	auto host_struct = gethostbyname(host.c_str());
	if (host_struct == nullptr) {
		throw socket_exception("Cannot resolve host");
	}
	auto ip_address = ((in_addr *)host_struct->h_addr_list[0])->s_addr;
	return ip_address;
}

std::string ssl_client::ip_to_string(unsigned int ip) {
	char data[64];
	inet_ntop(AF_INET, &ip, data, INET_ADDRSTRLEN);
	return data;
}

void ssl_client::load_certificates(SSL_CTX *context, const std::string &certificate, const std::string &key) {
	if (SSL_CTX_use_certificate_file(context, certificate.c_str(), SSL_FILETYPE_PEM) <= 0) {
		throw ssl_exception("Can't load certificate");
	}
	if (SSL_CTX_use_PrivateKey_file(context, key.c_str(), SSL_FILETYPE_PEM) <= 0) {
		throw ssl_exception("Can't load certificate");
	}
	if (!SSL_CTX_check_private_key(context)) {
		throw ssl_exception("Private key does not match public certificate");
	}
}

SSL_CTX *ssl_client::init_context() {
	OpenSSL_add_all_algorithms();
	SSL_load_error_strings();
	const auto method = SSLv23_client_method();
	const auto context = SSL_CTX_new(method);
	if (context == nullptr) {
		throw ssl_exception("Can't init ssl context");
	}
	return context;
}

int ssl_client::create_socket() {
	unsigned ip_address = host_to_ip(_host.c_str());

	sockaddr_in _sockaddr {
		AF_INET,
		htons(_port),
		{ip_address},
		{0}
	};

	int _socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket < 0) {
		throw socket_exception("Error on socket creation");
	}

	int _connect = connect(_socket, reinterpret_cast<sockaddr *>(&_sockaddr), sizeof(_sockaddr));
	if (_connect < 0) {
		throw socket_exception("Error on connection");
	}

	return _socket;
}

std::vector<char> ssl_client::get_package(const std::string &filename, size_t package_number) {
	std::ifstream file(filename, std::ifstream::binary | std::ios::out);
	std::vector<char> buffer(PACKAGE_SIZE);
	if (file) {
		file.seekg(0, std::ios::end);
		size_t end = file.tellg();
		file.seekg(package_number * PACKAGE_SIZE, std::ios::beg);
		size_t distance_to_end = end - file.tellg();
		if (distance_to_end < PACKAGE_SIZE) {
			buffer.resize(distance_to_end);
			file.read(buffer.data(), distance_to_end);
		} else {
			file.read(buffer.data(), PACKAGE_SIZE);
		}
		file.close();
	}
	return buffer;
}
